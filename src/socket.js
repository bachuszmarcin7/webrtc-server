const socket = require("socket.io");

const io = socket();

const clients = {}; //Disctionary that hold connected clients
const rooms = []; //Array that hold existing rooms

function setup(http, https)
{
    io.attach(http);
    io.attach(https);
}

io.on("connection", socket =>
{
    //Lobby

    socket.on("userConnected", userData => { addUser(socket, userData) });
    socket.on("userDisconnected", () => { userDisconnect(socket); });
    socket.on("getUsersOnline", () => { sendUsersList(socket); });

    //Room

    socket.on("getRoomUsers", () => { sendUsersInRoom(io, socket); });
    socket.on("roomCreate", data => { userCreateRoom(socket, data); });
    socket.on("roomJoin", roomName => { userJoinRoom(socket, roomName); });
    socket.on("leave", () => { userLeave(socket); });

    //Messaging

    socket.on("messageToUser", message => { sendMessageToUser(socket, message); });
    socket.on("messageToRoom", message => { sendMessageToRoom(socket, message); });

    //Disconnecting

    socket.on("disconnect", () => { userDisconnect(socket); });
});

// #region Functions

function addUser(socket, userData) //Add client to client list
{
    clients[socket.id] = { socket: socket.id, platform: userData.platform, name: userData.userName, room: "" };

    socket.broadcast.emit("userConnected", clients[socket.id]);
}

function sendUsersList(socket) //Sends all connected to server users list
{
    socket.emit("usersOnline", clients);
}

function sendUsersInRoom(io, socket) //Sends list of users in the same room as client
{
    let roomName = clients[socket.id].room;

    socket.emit("usersList", io.sockets.adapter.rooms[roomName].sockets);
}

function sendMessageToUser(socket, message) //Send message to specific user
{
    user = message.user;
    message.user = socket.id; //Replace message user id with id of sender

    socket.broadcast.to(user).emit("messageToUser", message);
}

function sendMessageToRoom(socket, message) //Send message to room where user is connected
{
    room = clients[socket.id].room;

    socket.broadcast.to(room).emit("messageToRoom", message);
}

function userCreateRoom(socket, data) //Create room
{
    let roomName = data.roomName;

    if (checkIfRoomExists(roomName))
    {
        socket.emit("roomExists", roomName);
        return;
    }

    rooms.push({ roomName: roomName, maxUsers: data.maxUsers });

    socket.join(roomName);
    socket.emit("roomCreated", roomName);
    socket.broadcast.emit("userCreatedRoom", { socket: socket.id, room: roomName });

    clients[socket.id].room = roomName;
}

function userJoinRoom(socket, roomName) //Join Room
{
    if (checkIfRoomExists(roomName) === false)
    {
        socket.emit("roomDoesNotExists", roomName);
        return;
    }

    let maxClients = getRoomMaxUsers(roomName);
    let clientsInRoom = getClientsInRoom(roomName);

    if (clientsInRoom < maxClients)
    {
        socket.join(roomName);
        socket.emit("roomJoined", roomName);
        socket.broadcast.emit("userJoinedRoom", { socket: socket.id, room: roomName });
        socket.broadcast.to(roomName).emit("messageToRoom", { type: "userJoined", user: socket.id});

        clients[socket.id].room = roomName;
    }
    else
    {
        socket.emit("roomFull", roomName);
        return;
    }
}

function userLeave(socket) //Leave room
{
    let roomName = clients[socket.id].room;

    socket.broadcast.to(roomName).emit("connectionState", { user: socket.id, content: { type: "connectionClosed" } }); //Get other clients notified

    socket.leave(roomName);
    socket.broadcast.emit("userLeftRoom", { socket: socket.id, room: "" });

    if (getClientsInRoom(roomName) === 0) { removeRoom(roomName); } //If client was the last user in room, remove room

    clients[socket.id].room = "";
}

function userDisconnect(socket) //Disconnect client
{
    if (clients[socket.id])
    {
        if (clients[socket.id].room !== "") { userLeave(socket); } //If client was in a room, make sure that he leave it

        delete clients[socket.id];

        socket.broadcast.emit("userDisconnected", socket.id);
    }
}

//#endregion ----------------------------------------------------------------------------------------------------------

// #region Helpers

function getRoomMaxUsers(roomName) //Loop rooms array and return maxUsers value
{
    let index = rooms.findIndex(room => room.roomName === roomName);

    return rooms[index].maxUsers;
}

function removeRoom(roomName) //Remove room from rooms array
{
    for (let i = 0; i < rooms.length; i++)
    {
        if (rooms[i].roomName === roomName)
        {
            rooms.splice(i, 1);
        }
    }
}

function checkIfRoomExists(roomName) //Loop rooms array and checks if room exists
{
    for (let i = 0; i < rooms.length; i++)
    {
        if (rooms[i].roomName === roomName)
        {
            return true;
        }
    }

    return false;
}

function getClientsInRoom(roomName) //Returns amount of clients connected to room
{
    let clientsInRoom = io.sockets.adapter.rooms[roomName];
    return clientsInRoom ? Object.keys(clientsInRoom.sockets).length : 0;
}

//#endregion ----------------------------------------------------------------------------------------------------------

exports.setup = setup;