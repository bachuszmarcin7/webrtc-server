const express = require("express");
const https = require('https');
const path = require('path');
const fs = require('fs');

const app = express();

const HTTP_port = process.env.PORT || 80;
const HTTPS_port = process.env.PORT || 443;

app.use(express.static(path.join(__dirname, '../build')), function (req, res, next)
{
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', function (req, res)
{
    res.sendFile(path.join(__dirname, '../my-app/build/index.html'));
});

options =
{
    key: fs.readFileSync("./ssl/key.pem"),
    cert: fs.readFileSync("./ssl/cert.pem"),
    passphrase: "test"
};

const HTTP_server = app.listen(HTTP_port, () => { console.log('Http server listening on the port: ' + HTTP_port); })
const HTTPS_server = https.createServer(options, app).listen(HTTPS_port, () => { console.log('Https server listening on the port: ' + HTTPS_port); });

exports.HTTP_server = HTTP_server;
exports.HTTPS_server = HTTPS_server;