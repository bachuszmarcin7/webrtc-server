var server = require("./src/server");
var socket = require("./src/socket");

socket.setup(server.HTTP_server, server.HTTPS_server);