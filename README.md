# WebRTC-Server

Simple server to signaling and user managing created with Node.js and Express.js.  
Application builds can be tested at: https://webrtc-multiplatform-master.herokuapp.com/  

How to deploy:

-Put aplication build inside project  
-Open console in project folder  
-Type "npm i" to download required node modules  
-Type "npm start"  
-Connect to your server adress (https)  

If you are hosting on your local machine then simply connect to https://localhost/
